import {Injectable, OnInit} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

declare let google: any;

@Injectable()
export class ChartService {

  onLoadChartSubscribe: BehaviorSubject<any> = new BehaviorSubject<any>([]);

  constructor() {
    google.charts.load('current', {packages: ['corechart', 'line']});
  }

  InitChartLine(elementId: string, dataChart: any[] = [], options: any): void {
    const func = (optionsP) => {
      const datatable = new google.visualization.DataTable();
      datatable.addColumn('number', 'X');
      datatable.addColumn('number', 'Valor Suma');
      datatable.addRows(dataChart);
      const chart = new google.visualization.LineChart(document.getElementById(elementId));
      chart.draw(datatable, optionsP);
    };
    const callback = () => func(options);
    google.charts.setOnLoadCallback(callback);
  }
}
