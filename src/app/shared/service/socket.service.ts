import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import * as io from 'socket.io-client';
import {ResponseSum} from '../model/responseSum';

@Injectable()
export class SocketService {
  private socket;

  /**
   * Nos conectamos al socket
   */
  public initSocket(): void {
    this.socket = io(environment.serverSumUrl);
  }

  /**
   * Devuelve el valor de la suma retornado por el WebSocket y el id del cliente.
   * @returns {Observable<any>}, Mediante el observador notificamos el resultado
   * a cualquiera que este subscripto
   */
  public onSumResult(): Observable<ResponseSum> {
    return new Observable<ResponseSum>(observer => {
      this.socket.on('sumResult', (data: ResponseSum) => observer.next(data));
    });
  }

  /**
   * Devuelve el Id del cliente (socket), creado en el Web Socket, Mediante el observador
   * notificamos cada vez que un nuevo usuario se una al Web Socket.
   * @returns {Observable<any>}, Mediante el observador notificamos el resultado
   * a cualquiera que este subscripto
   */
  public onJoinNewUser(): Observable<any> {
    return new Observable<any>(observer => {
      this.socket.on('joinNewUser', (data: string) => observer.next(data));
    });
  }

  /**
   * Devuelve el Id del cliente (socket), creado en el Web Socket, Mediante el Observador
   * notificamos cada vez que un cliente se DESCONECTE del Web Socket.
   * @returns {Observable<any>}
   */
  public onLeaveUser(): Observable<any> {
    return new Observable<any>(observer => {
      this.socket.on('leaveUser', (data: string) => observer.next(data));
    });
  }
}
