import {Component} from '@angular/core';
import {SocketService} from './shared/service/socket.service';
import {Client} from './shared/model/client';
import {UtilityService} from './shared/service/utility.service';
import {ResponseSum} from './shared/model/responseSum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  /*
  * Configuracion inical para el chart
  */
  public configChartDefault: any = {
    hAxis: {
      title: 'Tiempo'
    },
    vAxis: {
      title: 'Valores'
    },
    backgroundColor: '#f1f8e9',
    colors: ['blue']
  };
  /*
  * List de clientes que se conecten al socket
  */
  public connectedClients: Client[] = [];

  /*
  Creamos un prefijo para el id del chart
   */
  prefixChartElement: String = 'chart-line';
  seconds = 1;

  constructor(private socketService: SocketService, private utilityService: UtilityService) {
    this.initIoConnection();
  }

  private initIoConnection(): void {
    this.socketService.initSocket();

    /**
     * Actualizamos los datos del Chart Line con los datos que nos envia el socket.
     */
    this.socketService.onSumResult()
      .subscribe((data: ResponseSum) => {
        const indexClientFind = this.connectedClients.findIndex(x => x.ClientId === `${this.prefixChartElement}-${data.ClientId}`);
        const connectedClientFind = this.connectedClients[indexClientFind];
        if (indexClientFind > -1 && !connectedClientFind.Removed) {
          const dataChartCopy = Object.assign([], connectedClientFind.Data);
          if (dataChartCopy.length > 10) {
            dataChartCopy.shift();
          }
          dataChartCopy.push([this.seconds, data.Value]);
          this.connectedClients[indexClientFind].Data = dataChartCopy;
          this.seconds++;
        }
      });

    this.socketService.onJoinNewUser().subscribe((clientId: string) => {
      this.addNewClient(clientId);
    });

    this.socketService.onLeaveUser().subscribe((clientId: string) => {
      this.removeChartClientDisconnect(clientId);
    });
  }

  /**
   * Cada vez que se una un nuevo cliente al socket, se agrega al array {@link connectedClients},
   * para renderizar otro componente (ChartComponent) adicional en la vista.
   * @param {string} clientId, es necesario para posteriormente identificar cual es la grafica
   * de cada cliente.
   */
  private addNewClient(clientId: string) {
    const client = new Client();
    client.ClientId = `${this.prefixChartElement}-${clientId}`;
    client.Data = [];
    // Copiamos el objeto de configuración con el fin de no modificar el original,
    // en futuras modificaciones.
    client.ConfigChart = Object.assign({}, this.configChartDefault);
    client.Removed = false;
    this.connectedClients.push(client);
  }

  /**
   * Elimina un cliente del array de clientes conectados {@link connectedClients}.
   * Se realizara una espera de 5 segundos para que graficamente se vea una linea roja.
   * @param {string} clientId
   * @returns {Promise<void>}
   */
  private async removeChartClientDisconnect(clientId: string): Promise<void> {
    // Buscamos en el array connectedClients el clientId a eliminar del mismo.
    const indexClientFind = this.connectedClients.findIndex(x => x.ClientId === `${this.prefixChartElement}-${clientId}`);
    const connectedClientFind = this.connectedClients[indexClientFind];
    connectedClientFind.Removed = true;
    connectedClientFind.Data = [];
    // Cambiamos el color del linea del grafico
    connectedClientFind.ConfigChart['colors'] = ['red'];
    for (let i = 1; i < 6; i++) {
      const dataChartCopy = Object.assign([], connectedClientFind.Data);
      // Colocando los valores en 50 se simula una linea roja en la mitad del grafico
      dataChartCopy.push([this.seconds + i, 50]);
      connectedClientFind.Data = dataChartCopy;
      await this.utilityService.sleep(1000);
    }
    // Eliminamos el cliente luego de 5 segundos.
    this.connectedClients.splice(indexClientFind, 1);
  }
}
