import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ChartComponent} from './chart/chart.component';
import {ChartService} from './chart/chart.service';
import {SocketService} from './shared/service/socket.service';
import {UtilityService} from './shared/service/utility.service';

@NgModule({
  declarations: [
    AppComponent,
    ChartComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [ChartService, SocketService, UtilityService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
