import {Injectable} from '@angular/core';

@Injectable()
export class UtilityService {
  /**
   * Simulamos un ThreadSleep Async, con el fin de que espere por el tiempo en milisegundos
   * pasado como parametro
   * @param {number} ms, milisegundos que se simulara como espera.
   * @returns {Promise<any>}
   */
  public sleep(ms: number): Promise<any> {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}
