import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ChartService} from './chart.service';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit, OnChanges {

  @Input() dataChart: any[];
  @Input() config: any;
  @Input() elementId: string;

  constructor(private chartService: ChartService) {
  }

  ngOnInit() {
    this.chartService.InitChartLine(this.elementId, this.dataChart, this.config);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.dataChart !== undefined) {
      this.dataChart = changes.dataChart.currentValue;
      this.chartService.InitChartLine(this.elementId, this.dataChart, this.config);
    }
  }
}
